# parser.py
from .tokens import Token, TokenType
from .lexer import Lexer
from .tree import Node, BinOp, Number, UnarOp, EqOp, Var


class ParserException(Exception):
    ...


class Parser:

    global_memory = {'*': 0}
    memory_to_use = global_memory
    memory_levels = [global_memory]

    var_to_write = ''
    def __init__(self):
        self.current_token: Token = None
        self.lexer = Lexer()

    def check_type(self, type_: TokenType):
        if self.current_token.type == type_:
            self.current_token = self.lexer.next()
            return
        raise ParserException(f"Invalid token order. Expected {type_}, Received {self.current_token}")

    def term(self) -> Node:
        ops = [TokenType.DIV, TokenType.MUL]
        result = self.factor()
        while self.current_token.type in ops:
            token = self.current_token
            match token.type:
                case TokenType.DIV:
                    self.check_type(TokenType.DIV)
                    # result /= self.term()
                case TokenType.MUL:
                    self.check_type(TokenType.MUL)
                    # result *= self.term()
            result = BinOp(result, token, self.factor())
        return result

    def expr(self) -> Node:
        ops = [TokenType.PLUS, TokenType.MINUS, TokenType.EQ]
        result = self.term()
        while self.current_token.type in ops:
            token = self.current_token
            match token.type:
                case TokenType.PLUS:
                    self.check_type(TokenType.PLUS)
                    # result += self.term()
                case TokenType.MINUS:
                    self.check_type(TokenType.MINUS)
                    # result -= self.term()
                case TokenType.EQ:
                    self.check_type(TokenType.EQ)
            result = BinOp(result, token, self.term())
        return result

    def init_parser(self, s: str) -> float:
        self.lexer.init_lexer(s)
        self.current_token = self.lexer.next()

    def factor(self) -> Node:
        token = self.current_token
        ops = [TokenType.MINUS, TokenType.PLUS, TokenType.NUMBER, TokenType.LPAREN, TokenType.RPAREN, TokenType.VARIABLE]
        # print(token.type)
        # result =
        while self.current_token.type in ops:
            token = self.current_token
            if token.type == TokenType.VARIABLE:
                self.var_to_write = self.current_token.value
                self.check_type(TokenType.VARIABLE)
                self.check_type(TokenType.EQ)
                return EqOp(Var(self.memory_to_use, token.value), self.expr())
            if token.type == TokenType.MINUS:
                self.check_type(TokenType.MINUS)
                return UnarOp(token, self.factor())
            if token.type == TokenType.PLUS:
                self.check_type(TokenType.PLUS)
                return UnarOp(token, self.factor())

            if token.type == TokenType.NUMBER:
                self.check_type(TokenType.NUMBER)
                return Number(token)
            if token.type == TokenType.LPAREN:
                self.check_type(TokenType.LPAREN)
                result = self.expr()
                self.check_type(TokenType.RPAREN)
                return result
            else:
                self.logic()
            raise ParserException(f"invalid pactor: {token.type}")

    def logic(self) -> Node:
        token = self.current_token
        print(token.type)
        if token.type == TokenType.LOGIC_START:
            self.check_type(TokenType.LOGIC_START)
            self.memory_to_use['*'] += 1
            self.memory_to_use['*' + str(self.memory_to_use['*'])] = {'*': 0}
            self.memory_to_use = self.memory_to_use['*' + str(self.memory_to_use['*'])]
            self.memory_levels.append(self.memory_to_use)
            result = self.expr()
            self.check_type(TokenType.LOGIC_END)
            self.memory_levels.pop()
            self.memory_to_use = self.memory_levels[-1]
            return result

        raise ParserException(f"invalid pactor: {token.type}")

