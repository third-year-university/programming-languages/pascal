from plib.interpreter import Interpreter
from plib.parser import Parser
import pytest


class TestInterpreter:

    def test_interpreter(self):
        interp = Interpreter()
#         interp.eval("""
#             BEGIN
#     y: = 2;
#     BEGIN
#         a := 3;
#         a := a;
#         b := 10 + a + 10 * y / 4;
#         c := a - b
#     END;
#     x := 11;
# END.
#         """)
#         assert interp.eval("2+2") == 4
#         assert interp.eval("2-2") == 0
#         assert interp.eval(" 2 +     3") == 5
#         assert interp.eval("22 +     323") == 345
#         assert interp.eval("0.5 + 2") == 2.5
#         assert interp.eval("2 * 2") == 4
#         assert interp.eval("2 / 2") == 1
#         assert interp.eval("2 + 2 + 3 + 3") == 10
#         assert interp.eval("2 * 2 * 2") == 8
#         assert interp.eval("2 + 2 * 3 + 3") == 11
#         assert interp.eval("2") == 2.0
#         assert interp.eval("(2 + 2) * 3 + 3") == 15.0
#         assert interp.eval("(2 + 2) * (3 + (3 * 3))") == 48.0
#         assert interp.eval("-2") == -2
#         assert interp.eval("+2") == 2
#         assert interp.eval("--2--3") == 5.0
#         assert interp.eval("-(2 + 3)") == -5
